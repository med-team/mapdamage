mapdamage (2.2.2+dfsg-1) unstable; urgency=medium

  * New upstream version 2.2.2+dfsg
  * use_debian_packaged_seqtk.patch: unfuzz.
  * d/control: add myself to uploaders.
  * d/control: declare compliance to standards version 4.7.0.
  * use_debian_packaged_seqtk.patch: declare forwarding not-needed.

 -- Étienne Mollier <emollier@debian.org>  Sun, 25 Aug 2024 18:37:56 +0200

mapdamage (2.2.1+dfsg-3) unstable; urgency=medium

  * Fix watch file
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 24 Nov 2022 12:22:34 +0100

mapdamage (2.2.1+dfsg-2) unstable; urgency=medium

  * Fix watchfile to detect new versions on github
  * Standards-Version: 4.6.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 12 Oct 2021 07:30:58 +0200

mapdamage (2.2.1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Submit, Repository,
    Repository-Browse (routine-update)
  * d/rules: Improved post-build cleansing

 -- Steffen Moeller <moeller@debian.org>  Tue, 30 Jun 2020 00:19:37 +0200

mapdamage (2.2.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Remove trailing whitespace in debian/changelog
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g
  * Set upstream metadata fields: Bug-Database.
  * Fix and enhance build time and autopkgtest

 -- Andreas Tille <tille@debian.org>  Fri, 13 Dec 2019 14:01:31 +0100

mapdamage (2.1.1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
    - eliminates two patches
      + na.rm
      + import
    - rescale_test.py no longer available - simpler build process
  * Standards-Version: 4.4.1

 -- Steffen Moeller <moeller@debian.org>  Sat, 30 Nov 2019 18:52:19 +0100

mapdamage (2.1.0+dfsg-1) unstable; urgency=medium

  * New upstream version
    Closes: #936989
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Trim trailing whitespace.
  * buildsystem=pybuild
  * python3-pysam <!nocheck>
  * Deactivate rescale_test.py in autopkgtest
  * Test-Depends: gfortran

 -- Andreas Tille <tille@debian.org>  Tue, 01 Oct 2019 19:41:25 +0200

mapdamage (2.0.9+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Remove trailing whitespace in debian/changelog
  * Remove trailing whitespace in debian/rules

 -- Andreas Tille <tille@debian.org>  Sat, 08 Dec 2018 22:38:50 +0100

mapdamage (2.0.8+dfsg-2) unstable; urgency=medium

  [ Steffen Moeller ]
  * debian/upstream/metadata
    - added RRID
    - yamllint cleanliness

  [ Andreas Tille ]
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Drop needs-recommends restriction from autopkgtest (there are no
    Recommends anyway)
  * Do not parse d/changelog

 -- Andreas Tille <tille@debian.org>  Thu, 20 Sep 2018 09:40:01 +0200

mapdamage (2.0.8+dfsg-1) unstable; urgency=medium

  * New upstream version
  * debhelper 10
  * d/watch: version=4

 -- Andreas Tille <tille@debian.org>  Mon, 19 Jun 2017 09:37:29 +0200

mapdamage (2.0.6+dfsg-2) unstable; urgency=medium

  * Team upload

  [ Andreas Tille ]
  * Spelling

  [ Nadiya Sitdykova ]
  * Add simple tests for examples
  * Move pysam dependency from tests/control to control as it's needed
    not only for test, but for package
  * Add test checking stdin redirect
  * Add simple test to demonstrate the problems with installing
    r-cran-rcppgsl package
  * Edit path to seqtk in composition.py
    Closes: #859090
  * Fix typo in tests/control
  * Add patch to fix quantile call to remove NA and NaNs
  * Add dependency providing liblapack.so and libblas.so
    Closes: #859091
  * Add file with reference containing all types of nucleotides
  * Add information how to check R libraries and what to do in case of errors
    to README.test
  * Provide tests for examples from documentation
    Closes: #848326
  * Fixed typo in README.test

 -- Nadiya Sitdykova <rovenskasa@gmail.com>  Thu, 23 Mar 2017 21:34:41 -0700

mapdamage (2.0.6+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #833387)

 -- Andreas Tille <tille@debian.org>  Wed, 03 Aug 2016 22:46:13 +0200
